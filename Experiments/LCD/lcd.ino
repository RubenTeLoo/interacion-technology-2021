
// Pin assignments: https://herlaar.net/b3it/wp-content/uploads/2015/01/lcd.png


#include <LiquidCrystal.h>
const int rs = 12, en = 11, d4 = 5, d5 = 4, d6 = 3, d7 = 2;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);



const int ledPin = LED_BUILTIN;

void switchLowHighState(byte* state)
{
	if(* state == LOW)
	{
		* state = HIGH;
	}
	else if (* state  == HIGH) 
	{
		* state = LOW;
	}
	else
	{
		// todo: error
	}
}

void setup()
{
	// set up the LCD's number of columns and rows:
	lcd.begin(16, 2);

	lcd.print("hello, world!");
}

void loop()
{
	lcd.setCursor(0, 1);
	lcd.print(millis() / 100 );
}
