

const int ledPin = LED_BUILTIN;
const int buttonPin = PIN2;



byte ledState = LOW;


unsigned long nextLedStateSwitch = 0;
const unsigned long ledIntervalNorm = 1000;
const unsigned long ledIntervalFast =  100;



void switchLowHighState(byte* state)
{
	if(* state == LOW)
	{
		* state = HIGH;
	}
	else if (* state  == HIGH) 
	{
		* state = LOW;
	}
	else
	{
		// todo: error
	}
}

void setup()
{
	pinMode(ledPin, OUTPUT);
	pinMode(buttonPin, INPUT);
}

void loop()
{
	unsigned long currentMillis = millis();

	unsigned long currentIncrement = ledIntervalNorm;

	if (digitalRead(buttonPin))
	{
		currentIncrement = ledIntervalFast;
	}

	if(currentMillis > nextLedStateSwitch)
	{
		switchLowHighState(& ledState);

		nextLedStateSwitch = currentMillis + currentIncrement;
	}


	digitalWrite(ledPin, ledState);
}
