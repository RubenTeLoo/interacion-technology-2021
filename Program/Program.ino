

// Slow looks cool
#define Serial_BaudRate 250000
#define Echo_MaxDistance_cm 200



#define Seconds_to_Millis 1000ul
#define Minutes_to_Seconds 60ul
#define Minutes_to_Millis (Minutes_to_Seconds * Seconds_to_Millis)
#define Millis_to_Seconds_Divisor 1000ul


#define DistanceSensorDefaultSampleCount 5


#define SprayCanContent 2400


#define Door_Open 1
#define Door_Closed 0


#define LED_Max_Brightness 8





//##############################################################################
//######     Pin Assignments     ###############################################
//##############################################################################


// ### Analog pins
#define Pin_LightSens    A0 // Analog
#define Pin_Spray        A1 // *** Analog
#define Pin_Door         A2 // *** Analog
#define Pin_ButtonLadder A3 // Analog
#define Pin_OneWireBus   A4 // *** Analog
#define Pin_EchoTrigger  A5 // *** Analog



//##### Digital pins ###########################################################
// 1 (reserved by serial)
// 2 (reserved by serial)
#define Pin_Motion_sensor    2 // Interrupt
#define Pin_Button_Interrupt 3 // Interrupt, PWM (timer2)
#define Pin_LCD_d7           4
#define Pin_LCD_d6           5 // *** PWM (Timer0)
#define Pin_LED_Red          6 // PWM (Timer0)
#define Pin_LCD_d5           7
#define Pin_LCD_d4           8
#define Pin_LED_Yellow       9 // PWM (Timer1)
#define Pin_LED_Green       10 // PWM (Timer1)
#define Pin_LCD_en          11 // *** PWM (Timer2)
#define PIN_LCD_rs          12
#define Pin_EchoReceive     13 // Also Built in LED



//##############################################################################
//######     Stuff that shouldn't be necessary     #############################
//##############################################################################
#ifdef VSCode_Intellisense_Broken

// VSCode doesn't know about uint8_t and uint16_t for some reason.
#define uint8_t unsigned char
#define uint16_t unsigned short


#endif
//##############################################################################
//######     Libraries     #####################################################
//##############################################################################
#include <EEPROM.h>

#define StorageAddr_SprayShotRemain    0 // [uint32_t = 4 byte]
// 1,2,3 reserved
#define StorageAdr_SprayDelayManual    4 // [uint8_t = 1 byte] // Delay after manual request before spray
#define StorageAdr_SprayDelayAutomatic 5 // [uint8_t = 1 byte] // Delay after door close to spray
#define StorageAdr_SprayDelayBetween   6 // [uint8_t = 1 byte] // Delay between sprays



//##############################################################################
#include <LiquidCrystal.h>
LiquidCrystal lcd(PIN_LCD_rs, Pin_LCD_en, Pin_LCD_d4, Pin_LCD_d5, Pin_LCD_d6, Pin_LCD_d7);

// Symbols
#define LCD_Symbol_Degree char(223)


// Appears to not work anyway.
#define Serial_Symbol_Degree char(176)


//##############################################################################
#include <OneWire.h>

OneWire oneWire(Pin_OneWireBus);

//##############################################################################
#include <DallasTemperature.h>

DallasTemperature sensors(&oneWire);
DeviceAddress DevAdr_TempSensor;


//##############################################################################
#include <NewPing.h>

NewPing sonar(Pin_EchoTrigger, Pin_EchoReceive, Echo_MaxDistance_cm);



//##############################################################################
//######     Functions with default arguments     ##############################
//##############################################################################
// Because C++
void Sensor_Dist_Poll(byte samples = DistanceSensorDefaultSampleCount);





//###################################################################################
//## Things that need to be on top because C++ is too dumb to understand otherwise ##
//###################################################################################
uint32_t toiletStateEventTime = 0;

enum MenuPage : uint8_t
{
	/*  0 */ None, // LCD controlled by something else
	/*  1 */ IdleDisplay, // Show temperature and spray shots remaining
	/*  2 */ ResetSprayCounter,
	/*  3 */ SetManualDelay,
	/*  4 */ SetAutomaticDelay,
	/*  5 */ SetMultiSprayDelay,

	/*  6 */ ReadSensor_Dist,
	/*  7 */ ReadSensor_Light,
	/*  8 */ ReadSensor_Temp,
	/*  9 */ ReadSensor_Motion,
	/* 10 */ ReadSensor_Door,

	__FirstMenuPage = ResetSprayCounter,
	__LastMenuPage = ReadSensor_Door,
};

MenuPage CurrentMenuPage = IdleDisplay;



//##############################################################################
// parameters for toilet stuff
#define LightThreshold 450

#define number1Threshold (30ul * Seconds_to_Millis)
#define number2Threshold ( 3ul * Minutes_to_Millis)
#define usageExpire (60ul * Minutes_to_Millis)

#define motionTimeout (20ul * Seconds_to_Millis)

// After door closed, wait for motion sensor to reset.
#define doorClosedWait (8ul * Seconds_to_Millis)

// After door is open for this time, assume cleaning is progress.
#define CleaningThreshold (60ul * Seconds_to_Millis)

enum toiletStates : uint16_t
{
	Empty         = 0, // Door closed, light off, no motion for > 30s
	StatusUnknown = 1, // 
	OperatorMenu  = 2, // todo: Menu should implement a Timeout
	Spraying      = 3,

	DoorOpen      = 4, // Door open < 60s
	Cleaning      = 5, // Door open, light on, at least total > 60s

	ClosedUnknown = 6, // Door closed, light on, no motion for at most 30s
	Occupied      = 7, // Door closed, motion at least 5 seconds after door closed

	// bit 8 boundary, flags below
	
	Usage_Number1 = 16, // Door closed, light on, motion at least 5 seconds after door closed, total time > 30s & < 3m
	Usage_Number2 = 32, // Door closed, light on, motion at least 5 seconds after door closed, total time > 3m

	// Flag candidates
	// 16
	// 32
	// 64
	// 128

	__UsageFlags = 16 | 32,

};
toiletStates toiletState = StatusUnknown;



//##############################################################################
//######     Helper Functions     ##############################################
//##############################################################################


bool HasAnyFlag(uint16_t value, uint16_t flags)
{
	return (value & flags) != 0;
}


bool HasAllFlags(uint16_t value, uint16_t flags)
{
	return (value & flags) == flags;
}

bool HasOnlyFlags(uint16_t value, uint16_t flags)
{
	// Invert flags, so that only those bits of value that aren't in flags pass
	return (value & (~flags)) == 0;
}

bool EqualWithIgnoredFlags(uint16_t valueA, uint16_t valueB, uint16_t flags)
{
	return (valueA & (~flags)) == (valueB & (~flags));
}

// Print deviceAddress to Serial
void printAddress(DeviceAddress deviceAddress)
{
	for (uint8_t i = 0; i < 8; i++)
	{
		if (deviceAddress[i] < 16) Serial.print("0");
		Serial.print(deviceAddress[i], HEX);
	}
}

// Print Temperature to Serial
void printTemperature()
{
	printToSerial_Temp();
	lcd.leftToRight();
	printToLCD_Temp();
}

void printToSerial_Temp()
{
	float tempC = sensors.getTempC(DevAdr_TempSensor);
	Serial.print("Temp: ");
	Serial.print(tempC);
	Serial.print(" ");
	Serial.print(Serial_Symbol_Degree);
	Serial.println("C");
}

void printToLCD_Temp()
{
	float tempC = sensors.getTempC(DevAdr_TempSensor);
	lcd.print("Temp: ");
	lcd.print(tempC);
	lcd.print(" ");
	lcd.print(LCD_Symbol_Degree);
	lcd.print("C");
}



void switchLowHighState(byte* state)
{
	if(* state == LOW)
	{
		* state = HIGH;
	}
	else if (* state  == HIGH)
	{
		* state = LOW;
	}
	else
	{
		// todo: error
	}
}



//##############################################################################
//######     Sensors     #######################################################
//##############################################################################



uint32_t sensorPollLastMillis = 0;
#define sensorPollIntervalMillis (10 * Seconds_to_Millis)
void PSensors_Loop()
{
	auto currentMillis = millis();

	if (currentMillis - sensorPollLastMillis >= sensorPollIntervalMillis) // millis() overflow proof.
	{
		//noButtonInterrupt = true;

		sensorPollLastMillis = currentMillis;

		Serial.print("\n[PSensors_Loop] Measurements at time: ");
		Serial.print(currentMillis);
		Serial.println(" ms");

		Sensor_Door_Poll();
		Sensor_Light_Poll();
		Sensor_Dist_Poll();
		Sensor_Temp_Poll();

		//noButtonInterrupt = false;
	}
}

void Sensor_Door_Poll()
{
	Serial.print("Door: ");
	if(digitalRead(Pin_Door))
	{
		Serial.println("Open.");
	}
	else
	{
		Serial.println("Closed.");
	}
}

void Sensor_Temp_Poll()
{
	printToSerial_Temp();

	// Request updating the value.
	// It'll be at most 1 poll interval behind.
	sensors.requestTemperaturesByAddress(DevAdr_TempSensor);
}

void Sensor_Light_Poll()
{
	auto value = analogRead(Pin_LightSens);

	Serial.print("Light: ");
	Serial.print(value);
	Serial.print(" / 1023");

	if (value < LightThreshold)
	{
		Serial.println(" Dark.");
	}
	else
	{
		Serial.println(" Light.");
	}
}



void Sensor_Dist_Poll(byte samples)
{
	Serial.print("Echo: ");

	auto value_cm = sonar.ping_cm();

	Serial.print(value_cm);
	Serial.println("cm");
}

void Poll_Dist_To_LCD()
{
	auto value_cm = sonar.ping_median(10, 200) / US_ROUNDTRIP_CM;

	lcd.setCursor(0, 1);
	if (value_cm == NO_ECHO)
	{
		lcd.print("No Echo");
	}
	else
	{
		lcd.print(value_cm);
		lcd.print("cm  "); // extra spaces to compensate for 1, 2 or 3 digits
	}
}

double Poll_Dist_No_Library()
{
	// Trigger
	digitalWrite(Pin_EchoTrigger, HIGH);
	delayMicroseconds(10);
	digitalWrite(Pin_EchoTrigger, LOW);

	auto duration_us = pulseIn(Pin_EchoReceive, HIGH, 10000);
	auto distance_cm = duration_us * 0.034 / 2;

	return distance_cm;
}

/*
// With Timer2 interrupt
void Measure_Dist()
{
	sonar.ping_timer(interrupt_Echo);
}

void interrupt_Echo()
{
	if(sonar.check_timer())
	{
		auto value_cm = sonar.ping_result / US_ROUNDTRIP_CM;

		Serial.print("Echo: ");
		Serial.print(value_cm);
		Serial.println("cm");
	}
}
*/




//##############################################################################
//######     LED blink/fade     ################################################
//##############################################################################


byte fadeValue = 0;
byte fadeIndex = 0;
#define fadePinCount 3
const byte fadePins[fadePinCount] = {Pin_LED_Red, Pin_LED_Yellow, Pin_LED_Green};
void LED_Fade_Loop()
{
	if (millis() % 5 != 0) return;

	if (fadeValue < 128) // Don't make them super bright
	{
		fadeValue++;
	}
	else
	{
		fadeValue = 0;
		analogWrite(fadePins[fadeIndex], fadeValue);
		
		if(fadeIndex < fadePinCount - 1)
		{
			fadeIndex++;
		}
		else
		{
			fadeIndex = 0;
		}
	}
	analogWrite(fadePins[fadeIndex], fadeValue);
}


//##############################################################################
//######     Motion     ########################################################
//##############################################################################

//uint32_t motion_FirstSeenMillis = 0;
uint32_t motion_LastSeenMillis = 0;

void interrupt_Motion()
{
	uint32_t currentMillis = millis();
	//if(currentMillis - motion_LastSeenMillis > motionTimeout
	///* init only --> */ || motion_FirstSeenMillis == 0
	//)
	//{
	//	motion_FirstSeenMillis = currentMillis;
	//}

	motion_LastSeenMillis = currentMillis;

	Serial.print("Motion, TS=");
	Serial.println(currentMillis / Millis_to_Seconds_Divisor);
}


//##############################################################################
//######     Buttons     #######################################################
//##############################################################################
/*
This function is too complicated because we are forced to use an interrupt
by the requirements. Doing the debounce with loop() would be better, and would 
not have a chance to get stuck in the pressed state.

*/




bool buttonIsPressed = false;
bool buttonIsDebouncing = false;
byte buttonNumber = 0;

// Takes the length of the press in millis.
typedef void (*buttonCallback)(uint32_t);
buttonCallback buttonFunctions[3];



uint32_t buttonPressStart = 0;

uint32_t buttonLastInterrupt = 0;
#define buttonDebounceMillis 40

void interrupt_Button()
{
	//if(!buttonIsPressed && noButtonInterrupt) return; // Only suppress new press

	auto currentmillis = millis();

	if(buttonIsDebouncing)
	{
		if(currentmillis - buttonLastInterrupt >= buttonDebounceMillis)
		{
			// Debounce time expired.
			buttonIsDebouncing = false;
		}
		else
		{
			// Ignore the event.
			return;
		}
	}

	auto ladderLevel = analogRead(Pin_ButtonLadder);
	Serial.print("[Button] ");
	Serial.print(ladderLevel);

	if(buttonIsPressed)
	{
		auto buttonHoldTime = currentmillis - buttonPressStart;
		Serial.print(" Released button ");
		Serial.print(buttonNumber);
		Serial.print(" after: ");
		Serial.print(buttonHoldTime);
		Serial.print("ms");

		if(buttonHoldTime > 10 * Seconds_to_Millis)
		{
			// Too long: doesn't make sense -> discard.
			Serial.print(" That's too long, discarding event.");

			buttonIsPressed = false;
			buttonIsDebouncing = true;
			buttonLastInterrupt = currentmillis;
			return;
		}

		buttonIsPressed = false;
		buttonIsDebouncing = true;
		buttonLastInterrupt = currentmillis;

		if(buttonNumber >= 1 && buttonNumber <= 3) // 0 means no button.
		{
			auto func = buttonFunctions[buttonNumber - 1];

			if (func != nullptr)
			{
				//Serial.print(" Executing function: ");
				//uint16_t pointerValue = (uint16_t) func;
				//Serial.print(pointerValue, HEX);

				func(buttonHoldTime);
			}
			else
			{
				//Serial.print(" no assigned function for button.");
			}
		}
		else
		{
			Serial.print(" Button number was invalid!");
		}
		Serial.println();

	}
	else
	{
		Serial.print(" Press");
		// Buttons: NONE,  |  One  |  Two  | Three
		// levels:        / \ 445 / \ 348 / \ 000 
		// thresholds:    550     410     175     

		if(ladderLevel > 550)
		{
			// Probably misfire

			buttonNumber = 0;
			buttonIsPressed = false;
			buttonIsDebouncing = true;
			buttonPressStart = 0;
			buttonLastInterrupt = currentmillis;

			Serial.println(" misfire.");
			return;
		}
		

		buttonIsPressed = true;
		buttonIsDebouncing = true;
		buttonPressStart = currentmillis;
		buttonLastInterrupt = currentmillis;
		
		if(ladderLevel > 410)
		{
			buttonNumber = 1;

			Serial.println(" 1.");
		}
		else if(ladderLevel > 175)
		{
			buttonNumber = 2;

			Serial.println(" 2.");
		}
		else
		{
			buttonNumber = 3;

			Serial.println(" 3.");
		}
	}
}


void button1(uint32_t duration)
{
	Spray_Number1_Manual();
	Serial.println("[Button] Manual activation of spray sequence!");
}


void test_button_loop()
{
	if(millis() % 10 == 0)
	{
		auto ladderLevel = analogRead(Pin_ButtonLadder);
		lcd.setCursor(0,0);
		lcd.print(ladderLevel);
		lcd.print(" ");
	}
}

//##############################################################################
//######     Spraying!     #####################################################
//##############################################################################
// todo: allow setting additional delay before spray.


int32_t sprayShotsRemain; // Estimate of shots remaining in the can.
byte spraysRequested = 0; // Amount of shots requested.

uint32_t spraySequenceStartTime = 0; // millis when sequence started.
uint8_t spraySequenceExtraDelaySeconds = 0; // Additional delay before spraying starts.

#define spraySequenceExtraDelay (spraySequenceExtraDelaySeconds * Seconds_to_Millis)

uint8_t spraySequenceSetting_ExtraDelaySeconds_Manual  = 0;
uint8_t spraySequenceSetting_ExtraDelaySeconds_Auto    = 0;
uint8_t spraySequenceSetting_ExtraDelaySeconds_Between = 0;

#define spraySequenceSetting_ExtraDelaySeconds_UpperBound 64

// The time in sequence after which we will not abort
// (to prevent the sprayer getting stuck open)
#define spraySequenceNoCancelTime (14 * Seconds_to_Millis)

// The approximate time at which the spray actually occurs.
#define spraySequenceSprayTime (14 * Seconds_to_Millis)

// Time in sequence after which spraying is definitely complete.
#define spraySequenceSprayDoneTime (18 * Seconds_to_Millis)

// Time in sequence before we allow another sequence.
// (Prevent confusing the sprayer's microcontroller)
#define spraySequenceCooldownTime (22 * Seconds_to_Millis)

bool spraySequenceSprayDone = true;

void Spray_Number1_Automatic()
{
	spraysRequested = 1;
	spraySequenceExtraDelaySeconds = spraySequenceSetting_ExtraDelaySeconds_Auto;
}

void Spray_Number1_Manual()
{
	if(spraysRequested == 0) spraysRequested = 1;
	else if (spraysRequested == 1) spraysRequested = 2;
	else Spray_Abort();

	spraySequenceExtraDelaySeconds = spraySequenceSetting_ExtraDelaySeconds_Manual;
}

void Spray_Number2()
{
	spraysRequested = 2;

	// Never manually requested.
	spraySequenceExtraDelaySeconds = spraySequenceSetting_ExtraDelaySeconds_Auto;
}

bool Spray_Abort()
{
	if(spraysRequested == 0) return true;
	auto currentMillis = millis();
	if(currentMillis - spraySequenceStartTime >= spraySequenceNoCancelTime)
	{
		// Not allowed to abort (because risk of getting sprayer stuck open)
		spraysRequested = 1;
		Serial.println("[Spray_Abort] Cannot abort spray sequence! (risk could get stuck) Finishing currnet shot...");
		return false;
	}
	else
	{
		spraysRequested = 0;
		spraySequenceStartTime = 0;
		Serial.println("[Spray_Abort] Aborted spray sequence!");
		CurrentMenuPage = IdleDisplay;
		return true;
	}
}

void Spray_Loop()
{
	if( spraysRequested == 0) 
	{
		digitalWrite(Pin_Spray, LOW);
		return;
	}

	auto currentMillis = millis();

	if(spraysRequested > 5)
	{
		Serial.print("[Spray_Loop] spraysRequested (");
		Serial.print(spraysRequested);
		Serial.println(") > 5, that makes no sense!");

		if(Spray_Abort())
		{
			return;
		}
	}




	if(spraySequenceStartTime == 0)
	{
		spraySequenceStartTime = currentMillis;
		spraySequenceSprayDone = false;
		Serial.print("Spray Sequence start: T-");
		Serial.println((spraySequenceExtraDelay + spraySequenceSprayTime) / Millis_to_Seconds_Divisor);

		CurrentMenuPage = None;
		lcd.clear();
		lcd.leftToRight();
		lcd.setCursor(0,0);
		
		//         ################ <- LCD width measuring stick
		lcd.print("Spray imminent!");

		return;
	}


	// Wait the additional delay
	if(currentMillis - spraySequenceStartTime >= spraySequenceExtraDelay && ! spraySequenceSprayDone)
	{
		digitalWrite(Pin_Spray, HIGH);
	}


	if (! spraySequenceSprayDone && currentMillis - spraySequenceStartTime >= spraySequenceExtraDelay + spraySequenceSprayDoneTime) // millis() overflow proof.
	{
		// Spraying completed

		digitalWrite(Pin_Spray, LOW);

		sprayShotsRemain--;

		EEPROM.put(StorageAddr_SprayShotRemain, sprayShotsRemain);

		Serial.print("Spraying completed. Shots remaining: ");
		Serial.println(sprayShotsRemain);

		spraySequenceSprayDone = true;

		lcd.setCursor(0,0);
		//         ################ <- LCD width measuring stick
		lcd.print("Spray cooldown ");

	}


	// Update countdown
	if(! (currentMillis - spraySequenceStartTime >= spraySequenceExtraDelay + spraySequenceSprayDoneTime))
	{
		// Countdown
		lcd.setCursor(0,1);
		lcd.print(((spraySequenceExtraDelay + spraySequenceSprayDoneTime) - (currentMillis - spraySequenceStartTime)) / Millis_to_Seconds_Divisor);
		lcd.print("s ");
	}
	else
	{
		// Cooldown
		lcd.setCursor(0,1);
		lcd.print(((spraySequenceExtraDelay + spraySequenceCooldownTime) - (currentMillis - spraySequenceStartTime)) / Millis_to_Seconds_Divisor);
		lcd.print("s ");
	}

	if(currentMillis - spraySequenceStartTime >= spraySequenceExtraDelay + spraySequenceCooldownTime)
	{
		// Sprayer's MC had time forget it had just sprayed.

		spraysRequested--;
		spraySequenceStartTime = 0;

		if(spraysRequested > 0) spraySequenceExtraDelaySeconds = spraySequenceSetting_ExtraDelaySeconds_Between;

		CurrentMenuPage = IdleDisplay;
		SetToiletState(StatusUnknown);
	}
}

//##############################################################################
//######     Menu     ##########################################################
//##############################################################################



#define MenuIdleTimeout (20 * Seconds_to_Millis)
#define MenuRefreshTime (1 * Seconds_to_Millis)
#define MenuRefreshTimeFast 500

uint32_t menuLastInteraction = 0;

uint32_t menuSharedEventTime = 0;


void Menu_Loop()
{
	uint32_t currentmillis = millis();
	switch(CurrentMenuPage)
	{
		case None: return;

		case IdleDisplay:
		{
			if(currentmillis - menuSharedEventTime > MenuRefreshTime)
			{
				menuSharedEventTime = currentmillis;

				Menu_RenderPage();
			}
			return;
		}
		
		case ReadSensor_Dist:
		case ReadSensor_Light:
		case ReadSensor_Temp:
		case ReadSensor_Motion:
		case ReadSensor_Door:
		{
			if(currentmillis - menuSharedEventTime > MenuRefreshTimeFast)
			{
				menuSharedEventTime = currentmillis;

				Menu_RenderPage();
			}

			break;
		}

		default: break;
	}

	// Must update due to button interrupt causing page update between
	// first fetch of millis and timeout.
	currentmillis = millis();
	if(currentmillis - menuLastInteraction > MenuIdleTimeout)
	{
		CurrentMenuPage = IdleDisplay;

		Serial.print("[Menu] Timed out, returing to IdleDisplay TS=");
		Serial.println(currentmillis / Millis_to_Seconds_Divisor);
		
		SetToiletState(StatusUnknown);
	}
}


void Menu_ButtonNavigate(uint32_t duration)
{
	Spray_Abort();
	if(CurrentMenuPage == None) return; // Leave the other thing in control

	

	uint32_t currentmillis = millis();
	menuLastInteraction = currentmillis;

	if(CurrentMenuPage == __LastMenuPage)
	{
		CurrentMenuPage = IdleDisplay;
		SetToiletState(StatusUnknown);
	}
	else
	{
		uint8_t temp = (uint8_t) CurrentMenuPage;
		temp++;
		CurrentMenuPage = (MenuPage) temp;
	}
	Serial.print("[Menu] Navigate to page #");
	Serial.print(CurrentMenuPage);
	Serial.print(" T=");
	Serial.println(currentmillis);
	
	// Only clear lcd if actually changed.
	lcd.clear();
	Menu_RenderPage();
}

void Menu_ButtonApply(uint32_t duration)
{
	menuLastInteraction = millis();

	switch(CurrentMenuPage)
	{
		case None: return;
		case IdleDisplay:
		{
			Menu_ButtonNavigate(duration);
			return;
		}
		case ResetSprayCounter:
		{
			sprayShotsRemain = SprayCanContent;
			EEPROM.put(StorageAddr_SprayShotRemain, sprayShotsRemain);
			Serial.println("[Menu] Reset remaining spray count.");
			break;
		}
		case SetManualDelay:
		{
			if(spraySequenceSetting_ExtraDelaySeconds_Manual == 0) spraySequenceSetting_ExtraDelaySeconds_Manual = 1;
			spraySequenceSetting_ExtraDelaySeconds_Manual *= 2;
			if(spraySequenceSetting_ExtraDelaySeconds_Manual > spraySequenceSetting_ExtraDelaySeconds_UpperBound) spraySequenceSetting_ExtraDelaySeconds_Manual = 0;

			EEPROM.put(StorageAdr_SprayDelayManual, spraySequenceSetting_ExtraDelaySeconds_Manual);
			Serial.print("Set Delay_Manual to: ");
			Serial.println(spraySequenceSetting_ExtraDelaySeconds_Manual);
			break;
		}
		case SetAutomaticDelay:
		{
			if(spraySequenceSetting_ExtraDelaySeconds_Auto == 0) spraySequenceSetting_ExtraDelaySeconds_Auto = 1;
			spraySequenceSetting_ExtraDelaySeconds_Auto *= 2;
			if(spraySequenceSetting_ExtraDelaySeconds_Auto > spraySequenceSetting_ExtraDelaySeconds_UpperBound) spraySequenceSetting_ExtraDelaySeconds_Auto = 0;

			EEPROM.put(StorageAdr_SprayDelayAutomatic, spraySequenceSetting_ExtraDelaySeconds_Auto);
			Serial.print("Set Delay_Auto to: ");
			Serial.println(spraySequenceSetting_ExtraDelaySeconds_Auto);
			break;
		}
		case SetMultiSprayDelay:
		{
			if(spraySequenceSetting_ExtraDelaySeconds_Between == 0) spraySequenceSetting_ExtraDelaySeconds_Between = 1;
			spraySequenceSetting_ExtraDelaySeconds_Between *= 2;
			if(spraySequenceSetting_ExtraDelaySeconds_Between > spraySequenceSetting_ExtraDelaySeconds_UpperBound) spraySequenceSetting_ExtraDelaySeconds_Between = 0;

			EEPROM.put(StorageAdr_SprayDelayBetween, spraySequenceSetting_ExtraDelaySeconds_Between);
			Serial.print("Set Delay_Between to: ");
			Serial.println(spraySequenceSetting_ExtraDelaySeconds_Between);
			break;
		}
		case ReadSensor_Dist:
		case ReadSensor_Light:
		case ReadSensor_Temp:
		case ReadSensor_Motion:
		case ReadSensor_Door:
		{
			// Just update the display through RenderPage() below.
			break;
		}

		default: break;
	}
	Spray_Abort();
	Menu_RenderPage();
}

void Menu_RenderPage()
{
	lcd.leftToRight();
	lcd.setCursor(0,0);

	switch(CurrentMenuPage)
	{
		case IdleDisplay:
		{
			//         ################ <- LCD width measuring stick
			lcd.setCursor(0, 0);
			lcd.print("                ");

			lcd.setCursor(0, 0);
			printToLCD_Temp();

			lcd.setCursor(0, 1);
			//         ################ <- LCD width measuring stick
			//         Remain Spr: 2400
			lcd.print("Remain Spr:     ");
			lcd.setCursor(12, 1);
			lcd.print(sprayShotsRemain);
			return;
		}
		case ResetSprayCounter:
		{
			//         ################ <- LCD width measuring stick
			lcd.print("Reset SprayCount");
			lcd.setCursor(0, 1);
			//         ################ <- LCD width measuring stick
			//         Remaining: 2400
			lcd.print("Remaining:      ");
			lcd.setCursor(11, 1);
			lcd.print(sprayShotsRemain);

			return;
		}
		case SetManualDelay:
		{
			//         ################ <- LCD width measuring stick
			lcd.print("Manual Delay");
			lcd.setCursor(0, 1);
			lcd.print(spraySequenceSetting_ExtraDelaySeconds_Manual + (spraySequenceSprayTime / Millis_to_Seconds_Divisor));
			lcd.print("s ");

			return;
		}
		case SetAutomaticDelay:
		{
			//         ################ <- LCD width measuring stick
			lcd.print("Automatic Delay");
			lcd.setCursor(0, 1);
			lcd.print(spraySequenceSetting_ExtraDelaySeconds_Auto + (spraySequenceSprayTime / Millis_to_Seconds_Divisor));
			lcd.print("s ");

			return;
		}
		case SetMultiSprayDelay:
		{
			//         ################ <- LCD width measuring stick
			lcd.print("MultiSpray Delay");
			lcd.setCursor(0, 1);
			lcd.print(spraySequenceSetting_ExtraDelaySeconds_Between + (spraySequenceSprayTime / Millis_to_Seconds_Divisor));
			lcd.print("s ");

			return;
		}
		//######################################################################
		case ReadSensor_Dist:
		{
			//         ################ <- LCD width measuring stick
			lcd.print("Sensor/Distance ");
			lcd.setCursor(0, 1);
			Poll_Dist_To_LCD();

			return;
		}
		case ReadSensor_Light:
		{
			auto value = analogRead(Pin_LightSens);

			//         ################ <- LCD width measuring stick
			lcd.print("Sensor/ Light   ");
			lcd.setCursor(0, 1);
			
			//         ################ <- LCD width measuring stick
			//         1023 / 1023    
			lcd.print(value);
			lcd.print(" / 1023    ");

			return;
		}
		case ReadSensor_Temp:
		{
			//         ################ <- LCD width measuring stick
			lcd.print("Sensor/Temp     ");
			lcd.setCursor(0, 1);
			printToLCD_Temp();

			return;
		}
		case ReadSensor_Motion:
		{
			//         ################ <- LCD width measuring stick
			lcd.print("Senor/Motion    ");
			lcd.setCursor(0, 1);
			lcd.print("TODO             "); // todo

			return;
		}		
		case ReadSensor_Door:
		{
			//         ################ <- LCD width measuring stick
			lcd.print("Sensor/Door     ");
			lcd.setCursor(0, 1);
			if(digitalRead(Pin_Door))
			{
				//         ################ <- LCD width measuring stick
				lcd.print("Open           ");
			}
			else
			{
				lcd.print("Closed         ");
			}

			return;
		}

		default: return;
	}
}

//##############################################################################
//######     Toilet state     ##################################################
//##############################################################################


void Print_to_Serial_ToiletState(toiletStates state)
{
	auto flags = state & __UsageFlags;
	state = (toiletStates) (state & (~__UsageFlags));
	switch(state)
	{
		case Empty:         Serial.print("Empty"); break;
		case StatusUnknown: Serial.print("StatusUnknown"); break;
		case OperatorMenu:  Serial.print("OperatorMenu"); break;
		case Spraying:      Serial.print("Spraying"); break;
		case DoorOpen:      Serial.print("DoorOpen"); break;
		case Cleaning:      Serial.print("Cleaning"); break;
		case ClosedUnknown: Serial.print("ClosedUnknown"); break;
		case Occupied:      Serial.print("Occupied"); break;

		default:            Serial.print("UNKNOWN"); break;
	}

	if (flags)
	{
		if(HasAnyFlag(flags, Usage_Number2))
		{
			Serial.print(" | Number2");
		}
		if(HasAnyFlag(flags, Usage_Number1))
		{
			Serial.print(" | Number1");
		}
		if(! HasOnlyFlags(flags, __UsageFlags))
		{
			Serial.print(" | UNKNOWN_EXTRA_FLAGS");
		}
	}
}

bool SetToiletState(uint16_t newState)
{
	return SetToiletState((toiletStates) newState);
}

bool SetToiletState(toiletStates newState)
{
	if(toiletState == newState) return false;

	Serial.print("ToiletState: ");
	Print_to_Serial_ToiletState(newState);
	Serial.print(" ( <-- ");
	Print_to_Serial_ToiletState(toiletState);
	Serial.println(" )");

	toiletState = newState;

	if(toiletState == Spraying)
	{
		motion_LastSeenMillis = 0;
		//motion_FirstSeenMillis = 0;
	}

	UpdateStatusLEDs();
	return true;
}

uint8_t tempState = 0;
void SetTS_Loop_Case(uint8_t newValue)
{
	if(tempState == newValue) return;

	Serial.print("TS Loop Case: ");
	Serial.print(newValue);
	Serial.print(" ( <-- ");
	Serial.print(tempState);
	Serial.println(" )");

	tempState = newValue;
}

bool ls;

void Update_Toilet_State_Loop()
{
	uint32_t currentMillis = millis();
	auto lightValue = analogRead(Pin_LightSens);
	bool lightOn  = lightValue > LightThreshold;
	bool doorOpen = digitalRead(Pin_Door);

	if(lightOn != ls)
	{
		Serial.print("Light State: ");
		Serial.print(lightOn);
		Serial.print(" ( <-- ");
		Serial.print(ls);
		Serial.print(" )");
		Serial.println(lightValue);
		ls = lightOn;
	}

	//bool motionRecent = currentMillis - motion_LastSeenMillis < motionTimeout;

	// todo: use distance sensor

	if(spraysRequested > 0)
	{
		SetToiletState(Spraying);
	}
	else if(CurrentMenuPage != None && CurrentMenuPage != IdleDisplay)
	{
		SetToiletState(OperatorMenu);
	}
	else
	{
		// Determine current state

		if(doorOpen)
		{
			// Will only change with colsing of door.
			if(toiletState == Cleaning) return;

			// Keep usage flags, and set door to open.
			if(SetToiletState((toiletState & __UsageFlags) | DoorOpen))
			{
				// State was new.
				toiletStateEventTime = currentMillis;
			}
			else
			{
				// already in this state.
				if(currentMillis - toiletStateEventTime > CleaningThreshold
				&& lightOn)
				{
					// Erase pending sprays
					SetToiletState(Cleaning);
				}
				else
				{
					// Light off --> definitely not cleaning.
					// Stay in DoorOpen.
				}
			}
		} //####################################################################
		else // Door is closed #################################################
		{ //####################################################################
			if(EqualWithIgnoredFlags(toiletState, DoorOpen, __UsageFlags))
			{
				SetTS_Loop_Case(0);

				// Door just closed
				toiletStateEventTime = currentMillis;

				// preserve flags in case door was opened shortly,
				// or a quick swap of occupants ocurred.
				SetToiletState((toiletState & __UsageFlags) | ClosedUnknown);

				// We need to wait at least some time before we know anything else.
				return;
			}

			if(EqualWithIgnoredFlags(toiletState, ClosedUnknown, __UsageFlags))
			{
				// Ensure sensors stabilize.
				if(currentMillis - toiletStateEventTime < doorClosedWait)
				{
					SetTS_Loop_Case(1);
					return;
				}
			}

			// Sensors are stable. #############################################
			if(lightOn)
			{
				if(EqualWithIgnoredFlags(toiletState, ClosedUnknown, __UsageFlags))
				{
					SetTS_Loop_Case(2);

					if(currentMillis - motion_LastSeenMillis < doorClosedWait)
					{
						// Motion was detected after the door closed.
						SetToiletState(Occupied | (__UsageFlags & toiletState));
					}
				}
				else if(HasAllFlags(toiletState, Usage_Number1))
				{
					SetTS_Loop_Case(3);

					if(currentMillis - toiletStateEventTime > number2Threshold)
					{
						SetToiletState(Occupied | Usage_Number2);
					}
				}
				else if(HasAllFlags(toiletState, Usage_Number2))
				{
					SetTS_Loop_Case(4);

					// Just wait for user to leave.
				}
				else if(EqualWithIgnoredFlags(toiletState, Occupied, __UsageFlags))
				{
					SetTS_Loop_Case(5);

					// We already checked for Usage1 or 2, so we don't have any usage yet when we get to here.

					if(currentMillis - toiletStateEventTime > number1Threshold)
					{
						SetToiletState(Occupied | Usage_Number1);
					}
				}
				else
				{
					SetTS_Loop_Case(6);
					//Serial.println("Dead condition");
					SetToiletState(Empty);
				}
			}
			else // Light is off ###############################################
			{
				if(toiletState == Empty) return;

				// Check for visitor in the dark.
				if(EqualWithIgnoredFlags(toiletState, ClosedUnknown, __UsageFlags))
				{
					SetTS_Loop_Case(7);

					if(currentMillis - toiletStateEventTime > doorClosedWait)
					{
						if(currentMillis - motion_LastSeenMillis < doorClosedWait)
						{
							// Motion was detected after the door closed.
							SetToiletState((toiletState & __UsageFlags) | Occupied);
						}
						else
						{
							SetToiletState((toiletState & __UsageFlags) | Empty);
						}
					}
					// else wait.
				}
				else if(EqualWithIgnoredFlags(toiletState, Empty, __UsageFlags))
				{
					SetTS_Loop_Case(8);

					// Empty, sufficient time has expired, and we have usage flags.
					if(HasAllFlags(toiletState, Usage_Number2))
					{
						Serial.println("Request Number2");
						Spray_Number2();
					}
					else if(HasAllFlags(toiletState, Usage_Number1))
					{
						Serial.println("Request Number1");
						Spray_Number1_Automatic();
					}
				}
				else
				{
					SetTS_Loop_Case(9);

					/*
					if(SetToiletState(Empty))
					{
						Serial.println("Base condition");
					}
					*/
				}
			}

			
			if(EqualWithIgnoredFlags(toiletState, Occupied, __UsageFlags)
				&& (currentMillis - toiletStateEventTime > usageExpire))
			{
				// If we think the user has been here for an extremely long time something is probably amiss -> reset
				Serial.print("Toilet visit expired.");
				Serial.print(currentMillis);
				Serial.print("  ");
				Serial.print(toiletStateEventTime);
				Serial.print("  ");
				Serial.print(currentMillis - toiletStateEventTime);
				Serial.print("  ");
				Serial.println(usageExpire);
				toiletStateEventTime = currentMillis;
				SetToiletState(StatusUnknown);
			}
		}
	}
}

void UpdateStatusLEDs()
{
	auto filteredState = toiletState & (~__UsageFlags);
	switch(filteredState)
	{
		case Empty:
		case StatusUnknown:
		case ClosedUnknown:
		{
			analogWrite(Pin_LED_Green,  LED_Max_Brightness);
			analogWrite(Pin_LED_Red,    0);
			analogWrite(Pin_LED_Yellow, 0);
			break;
		}
		case Spraying:
		{
			analogWrite(Pin_LED_Green,  0);
			analogWrite(Pin_LED_Red,    LED_Max_Brightness);
			analogWrite(Pin_LED_Yellow, 0);
			break;
		}
		case OperatorMenu:
		{
			analogWrite(Pin_LED_Green,  LED_Max_Brightness);
			analogWrite(Pin_LED_Red,    LED_Max_Brightness);
			analogWrite(Pin_LED_Yellow, LED_Max_Brightness);
			break;
		}
		case DoorOpen:
		case Cleaning:
		{
			analogWrite(Pin_LED_Green,  LED_Max_Brightness);
			analogWrite(Pin_LED_Red,    0);
			analogWrite(Pin_LED_Yellow ,LED_Max_Brightness);
			break;
		}
		case Occupied:
		{
			analogWrite(Pin_LED_Green,  0);
			analogWrite(Pin_LED_Red,    0);
			analogWrite(Pin_LED_Yellow, LED_Max_Brightness);
			break;
		}
	}
}

//##############################################################################
//######     Setup     #########################################################
//##############################################################################
void setup()
{
	Serial.begin(Serial_BaudRate);
	Serial.println("[Setup] Start.");

	Serial.println("[Setup] Read EEPROM");

	EEPROM.get(StorageAddr_SprayShotRemain, sprayShotsRemain);
	Serial.print(" sprayShotsRemain: ");
	Serial.println(sprayShotsRemain);

	EEPROM.get(StorageAdr_SprayDelayManual, spraySequenceSetting_ExtraDelaySeconds_Manual);
	Serial.print(" DelayManual: ");
	Serial.println(spraySequenceSetting_ExtraDelaySeconds_Manual);

	EEPROM.get(StorageAdr_SprayDelayAutomatic, spraySequenceSetting_ExtraDelaySeconds_Auto);
	Serial.print(" DelayAuto: ");
	Serial.println(spraySequenceSetting_ExtraDelaySeconds_Auto);


	EEPROM.get(StorageAdr_SprayDelayBetween, spraySequenceSetting_ExtraDelaySeconds_Between);
	Serial.print(" DelayBetween: ");
	Serial.println(spraySequenceSetting_ExtraDelaySeconds_Between);




	// Set LCD columns and rows.
	lcd.begin(16, 2);
	lcd.print("Setup...");
	lcd.setCursor(0,0);

	// Find sensors
	sensors.begin();

	Serial.print("[Setup/OneWire] Found: ");
	Serial.print(sensors.getDeviceCount(), DEC);
	Serial.println(" devices.");



	if (!sensors.getAddress(DevAdr_TempSensor, 0)) Serial.println("[Setup/OneWire] Unable to find address for Device 0");

	//Serial.print("[Setup/OneWire] TempSensor Address: ");
	printAddress(DevAdr_TempSensor);
	Serial.println();
	sensors.requestTemperaturesByAddress(DevAdr_TempSensor);

	// Don't block.
	sensors.setWaitForConversion(false);


	// Temporary defaults:
	buttonFunctions[0] = button1;
	buttonFunctions[1] = Menu_ButtonNavigate;
	buttonFunctions[2] = Menu_ButtonApply;


	// Interrupt handlers should be setup before the interrupts are attached to pins.
	


	pinMode(Pin_LightSens, INPUT);
	pinMode(Pin_Door, INPUT);

	pinMode(Pin_Motion_sensor, INPUT);
	attachInterrupt(digitalPinToInterrupt(Pin_Motion_sensor), interrupt_Motion, RISING);


	pinMode(Pin_ButtonLadder, INPUT);
	pinMode(Pin_Button_Interrupt, INPUT);
	attachInterrupt(digitalPinToInterrupt(Pin_Button_Interrupt), interrupt_Button, CHANGE);


	pinMode(Pin_LED_Red,    OUTPUT);
	pinMode(Pin_LED_Yellow, OUTPUT);
	pinMode(Pin_LED_Green,  OUTPUT);

	pinMode(Pin_Spray, OUTPUT);


	pinMode(Pin_EchoTrigger, OUTPUT);
	pinMode(Pin_EchoReceive, INPUT);



	Serial.print("Test Eq: E|1, E, 12 = ");
	Serial.println(EqualWithIgnoredFlags(Empty | Usage_Number1, Empty, __UsageFlags));

	Serial.print("Test Eq: E|1, E, 12 = ");
	Serial.println(EqualWithIgnoredFlags(Empty | Usage_Number1, Empty, __UsageFlags));


	Serial.println(LightThreshold);
	Serial.println(number1Threshold);
	Serial.println(number2Threshold);
	Serial.println(usageExpire);
	Serial.println(motionTimeout);
	Serial.println(doorClosedWait);
	Serial.println(CleaningThreshold);

	Serial.println("[Setup] Done.");
}

void loop()
{
	//PSensors_Loop();

	Menu_Loop();

	Update_Toilet_State_Loop();

	Spray_Loop();

	delay(2);
}
